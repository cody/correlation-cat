# correlation-cat

**This is only a proof of concept**

Gather and concatenate all GitLab log entries that match a given correlation ID, from several nodes.

## DO THIS FIRST

You need to be able to access your VMs by name only. 

```
ssh gitlab-rails01
```

To do this, you'll need to update your configuration file at `~/.ssh/config`. Below is an example of what this may look like:

```
Host gitlab-rails01
	HostName 1.1.1.1
	IdentityFile ~/.ssh/id_rsa
	User bob
Host sidekiq01
	HostName 2.2.2.2
	IdentityFile ~/.ssh/id_rsa
	User bob
```

This keeps the script simpler, and should also make your day to day administration of your GitLab nodes much easier.

## Usage

To run this script you primarily need two things: a correlation ID (`x-request-id` response header) and a list of SSH hosts where your GitLab components are running. You will pass the correlation ID as an environment variable XID and the list of SSH hosts are arguments to the script (separated by spaces).

Example:

```
XID=abc123 ./correlation-cat gitlab-rails01 sidekiq01
```
